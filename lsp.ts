abstract class PostalAddress {
    address: string;
    city: string;
    country: string;
    abstract writeaddrss();
}

class GermanPostalAddress extends PostalAddress {
    writeaddrss() {
        console.log('german formatted address');
    }
}

class ItalyPostalAddress extends PostalAddress {
    writeaddrss() {
        console.log('italy formatted address');
    }
}

class AddressWriter {
    static printPostalAddress(address: PostalAddress) {
        address.writeaddrss();
    }
}

const german = new GermanPostalAddress();
const italy = new ItalyPostalAddress();
AddressWriter.printPostalAddress(german);
AddressWriter.printPostalAddress(italy);