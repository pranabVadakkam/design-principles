interface Printer {
    printDocument(document: Document);
}


interface Stapler {
    stapleDocument(document: Document, tray: number);
}


interface Copier {
    copyDocument();
}

class SimplePrinter implements Printer {
    public printDocument() {
        console.log('print document');
    }
}


class SuperPrinter implements Printer, Stapler, Copier {
    public copyDocument() {
        console.log('copy document');
    }

    public printDocument() {
        console.log('print document');
    }

    public stapleDocument() {
        console.log('copy document');
    }
}
const simple = new SimplePrinter();
const superPrint = new SuperPrinter();
simple.printDocument();
superPrint.copyDocument();
superPrint.printDocument();
superPrint.stapleDocument();