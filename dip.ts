interface Iwindow {
    open();
    close();
}

class CarWindow implements Iwindow {
    open() {
        console.log('car window is opened');
    }
    close() {
        console.log('car window is closed');
    }
}

class CarWindowSwitch {
    toggle: boolean;
    window: Iwindow;
    constructor(window: Iwindow) {
        this.window = window;
    }
    switch() {
        this.toggle ? this.window.close() : this.window.open();
        this.toggle = !this.toggle;
    }
}

const windows = new CarWindow();
const carwindow = new CarWindowSwitch(windows);
carwindow.switch();
carwindow.switch();
