abstract class Employee {
    name: string;
    constructor(name: string) {
        this.name = name;
    }
    abstract calculateBonus(salary: number);


}

class Permanent extends Employee {
    constructor(name: string) {
        super(name);
    }
    calculateBonus(salary: number) {
        return salary + 10000;
    }
}

class Temporary extends Employee {
    constructor(name: string) {
        super(name);
    }
    calculateBonus(salary: number) {
        return salary + 5000;
    }
}

const perm = new Permanent('ram');
const temp = new Temporary('sam');

console.log('permanane employee name is ' + perm.name + ' and bonus is '+ perm.calculateBonus(50000));
console.log('temporary employee name is ' + temp.name + ' and bonus is '+ temp.calculateBonus(50000));
