class User {
    userLogin: boolean;
    register() {
        console.log('user registered');
        this.login();
    }
    login() {
        console.log('user Logged');
        this.userLogin = true;
    }
}

class Logger {
    user: User;
    constructor(user: User) {
        this.user = user;
        this.logError();
    }
    logError() {
        if (!this.user.userLogin) {
            throw new Error('user Failed to Login');
        }
    }
}

class MailHandler {
    user: User;
    constructor(user: User) {
        this.user = user;
        this.sendMail();
    }

    sendMail() {
        if (this.user.userLogin) {
            console.log('Mail sent');
        }
    }
}

const user = new User();
user.register();
const log = new Logger(user);
const mail = new MailHandler(user);